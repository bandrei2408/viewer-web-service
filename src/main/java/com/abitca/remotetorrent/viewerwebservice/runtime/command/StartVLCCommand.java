package com.abitca.remotetorrent.viewerwebservice.runtime.command;

public class StartVLCCommand implements ICommand{

    private String vlcPath;
    private String moviePath;
    private boolean enterFullScreen;

    public StartVLCCommand(String moviePath){
        this("vlc", moviePath);
    }

    public StartVLCCommand(String vlcPath, String moviePath){
        this(vlcPath, moviePath, true);
    }

    public StartVLCCommand(String vlcPath, String moviePath, boolean enterFullScreen){
        this.vlcPath = vlcPath;
        this.moviePath = moviePath;
        this.enterFullScreen = enterFullScreen;
    }

    public String getCommandExec() {
        return vlcPath + " \"" + moviePath + "\" " + (enterFullScreen ? "-f" : "");
    }

}
