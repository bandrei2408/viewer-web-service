package com.abitca.remotetorrent.viewerwebservice.runtime;

import com.abitca.remotetorrent.viewerwebservice.runtime.command.ICommand;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;

public class RuntimeManager{

    private Runtime runtime;

    public RuntimeManager(){
        runtime = Runtime.getRuntime();
    }

    public void runCommand(ICommand com){
        try {
            runtime.exec(com.getCommandExec());
        } catch (IOException e) {
            LogManager.getLogger(RuntimeManager.class).fatal(e.getMessage());
        }
    }
}
