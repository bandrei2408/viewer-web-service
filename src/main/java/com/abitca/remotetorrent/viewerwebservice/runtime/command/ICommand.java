package com.abitca.remotetorrent.viewerwebservice.runtime.command;

public interface ICommand {

    String getCommandExec();

}
