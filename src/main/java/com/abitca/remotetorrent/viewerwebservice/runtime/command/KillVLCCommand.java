package com.abitca.remotetorrent.viewerwebservice.runtime.command;

public class KillVLCCommand implements ICommand{

    public String getCommandExec() {
        return "TaskKill /F /IM vlc.exe";
    }

}
