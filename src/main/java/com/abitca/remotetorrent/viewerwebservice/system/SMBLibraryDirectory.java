package com.abitca.remotetorrent.viewerwebservice.system;

import com.abitca.remotetorrent.viewerwebservice.utils.VideoValidator;
import org.apache.logging.log4j.LogManager;
import java.util.ArrayList;

import jcifs.smb.SmbFile;

public class SMBLibraryDirectory extends RemoteLibraryDirectory {

    public SMBLibraryDirectory(String ip, String username, String password, String path){
        super(ip, username, password, path);
    }

    @Override
    protected void buildConnectionUrl() {
        this.shareURL = new String("smb://" + this.username + ":" + this.password + "@" + this.ip + "/" + this.path + "/");
    }

    @Override
    public ArrayList<MovieFile> getMovieList() {
        ArrayList<MovieFile> movies = new ArrayList<>();
        try {
            SmbFile sharedFolder = new SmbFile(this.shareURL);
            movies  = getMovieFiles(sharedFolder);
        } catch(Exception ex){
            LogManager.getLogger(SMBLibraryDirectory.class).fatal(ex.getMessage());
        }
        return movies;
    }

    protected ArrayList<MovieFile> getMovieFiles(SmbFile smbFile) throws Exception{
        ArrayList<MovieFile> movies = new ArrayList<>();
        if(smbFile.isDirectory()){
            SmbFile smbFiles[] = smbFile.listFiles();
            for(SmbFile tmpSmbFile : smbFiles){
                 movies.addAll(getMovieFiles(tmpSmbFile));
            }
        } else {
            if(new VideoValidator().validate(smbFile.getName()) && smbFile.length() / (1000 * 1000) > 100) {
                movies.add(new MovieFile(smbFile, this));
            }
        }
        return movies;

    }
}
