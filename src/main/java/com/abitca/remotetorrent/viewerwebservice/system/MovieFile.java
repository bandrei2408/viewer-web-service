package com.abitca.remotetorrent.viewerwebservice.system;

import com.abitca.remotetorrent.viewerwebservice.webservice.endpoint.IJSONObject;
import jcifs.smb.SmbFile;
import org.apache.logging.log4j.LogManager;
import org.json.simple.JSONObject;
import java.nio.file.Path;
import java.security.MessageDigest;

public class MovieFile implements IJSONObject {

    public String name;
    public LibraryDirectory directory;
    public long size;
    public String path;
    public String hash;

    public MovieFile(String name, LibraryDirectory directory, long size, String path, String hash){
        this.name = name;
        this.directory = directory;
        this.size = size;
        this.path = path;
        this.hash = hash;
    }

    public MovieFile(Path filePath, LibraryDirectory directory){
        this.name = filePath.getFileName().toString();
        this.directory = directory;
        this.size = filePath.toFile().length() / (1000 * 1000);
        this.path = filePath.toString();
        try {
            this.hash = MessageDigest.getInstance("md5").digest(path.getBytes("UTF-8")).toString();
        } catch (Exception ex){
            LogManager.getLogger(MovieFile.class).fatal(ex.getMessage());
        }
    }

    public MovieFile(SmbFile smbFile, LibraryDirectory directory){
        this.name = smbFile.getName();
        this.directory = directory;
        this.path = smbFile.getUncPath();
        try {
            this.size = smbFile.length() / (1000 * 1000);
            this.hash = MessageDigest.getInstance("md5").digest(path.getBytes("UTF-8")).toString();
        } catch(Exception ex){
            LogManager.getLogger(MovieFile.class).fatal(ex.getMessage());
        }
    }

    public JSONObject toJSON(){
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("name", name);
        jsonObj.put("directory", directory.path);
        jsonObj.put("size", size);
        jsonObj.put("path", path);
        jsonObj.put("hash", hash);
        return jsonObj;
    }

}
