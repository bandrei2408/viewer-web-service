package com.abitca.remotetorrent.viewerwebservice.system;

import java.util.ArrayList;

public abstract class LibraryDirectory {

    protected String path;

    public abstract ArrayList<MovieFile> getMovieList();

}
