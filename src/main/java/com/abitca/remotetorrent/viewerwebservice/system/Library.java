package com.abitca.remotetorrent.viewerwebservice.system;

import java.util.ArrayList;
import java.util.Comparator;

public class Library {

    private ArrayList<MovieFile> movies;
    private ArrayList<LibraryDirectory> directories;

    public Library(ArrayList<LibraryDirectory> directories){
        this.directories = directories;
    }

    public void syncMovies(){
        movies = new ArrayList<>();
        directories.forEach(dir -> addMovies(dir.getMovieList()));
        sortMovies();
    }

    public void sortMovies(){
        movies.sort((m1, m2) -> m1.name.compareTo(m2.name));
    }

    public ArrayList<MovieFile> getMovies() {
        return movies;
    }

    public void addMovie(MovieFile movie){
        movies.add(movie);
    }

    public void addMovies(ArrayList<MovieFile> movies){
        this.movies.addAll(movies);
    }

    public MovieFile getMovieByHash(String hash){
        return movies.stream().filter( movie -> movie.hash.equals(hash)).findFirst().orElse(null);
    }

}
