package com.abitca.remotetorrent.viewerwebservice.system;

import java.util.ArrayList;

abstract public class RemoteLibraryDirectory extends LibraryDirectory {

    protected String ip;
    protected String username;
    protected String password;
    protected String shareURL;

    public RemoteLibraryDirectory(String ip, String username, String password, String path){
        this.ip = ip;
        this.username = username;
        this.password = password;
        this.path = path;
        buildConnectionUrl();
    }

    protected abstract void buildConnectionUrl();

    public abstract ArrayList<MovieFile> getMovieList();
}
