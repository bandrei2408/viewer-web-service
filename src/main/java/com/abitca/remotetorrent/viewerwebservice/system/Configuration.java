package com.abitca.remotetorrent.viewerwebservice.system;

import com.abitca.remotetorrent.viewerwebservice.utils.Utils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import java.util.ArrayList;

public class Configuration {

    private String VLCPath;

    private ArrayList<LibraryDirectory> directories;

    public Configuration(String configPath){
        configPath = configPath == null ? getClass().getClassLoader().getResource("default.conf").getPath() : configPath;
        JSONObject configurationObj = (JSONObject) JSONValue.parse(Utils.readFile(configPath));
        parseConfiguration(configurationObj);
    }

    private void parseConfiguration(JSONObject configurationObj){
        VLCPath = (String) configurationObj.get("VLCPath");
        directories = new ArrayList<LibraryDirectory>();
        JSONArray directoriesJSON = (JSONArray) configurationObj.get("library");
        for ( Object directoryObj : directoriesJSON ) {
            JSONObject directoryJSON = (JSONObject) directoryObj;
            String type = ((String) directoryJSON.get("type")).toLowerCase();
            String path = ((String) directoryJSON.get("path")).toLowerCase();
            if(type.equals("smbshare")){
                String ip = (String) directoryJSON.get("ip");
                String username = (String) directoryJSON.get("username");
                String password = (String) directoryJSON.get("password");
                directories.add(new SMBLibraryDirectory(ip, username, password, path));
            } else {
                directories.add(new LocalLibraryDirectory(path));
            }
        }
    }

    public ArrayList<LibraryDirectory> getDirectories() {
        return directories;
    }
}
