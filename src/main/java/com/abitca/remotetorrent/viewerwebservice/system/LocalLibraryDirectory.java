package com.abitca.remotetorrent.viewerwebservice.system;

import com.abitca.remotetorrent.viewerwebservice.utils.VideoValidator;
import org.apache.logging.log4j.LogManager;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class LocalLibraryDirectory extends LibraryDirectory {

    public LocalLibraryDirectory(String path){
        this.path = path;
    }

    public ArrayList<MovieFile> getMovieList() {
        ArrayList<MovieFile> movies = new ArrayList<>();
        try {
            Files.walk(Paths.get(path)).forEach(filePath -> {
                if (Files.isRegularFile(filePath)) {
                    try {
                        //@FIXME(2): Use another method to remove sample files instead of file size
                        if(new VideoValidator().validate(filePath.getFileName().toString()) && (filePath.toFile().length() / (1000 * 1000) > 100)){
                            movies.add(new MovieFile(filePath, this));
                        }
                    } catch (Exception ex){
                        LogManager.getLogger(LocalLibraryDirectory.class).warn("Skipped " + filePath + " (No Content Type)");
                    }
                }
            });
        } catch (Exception ex){
            LogManager.getLogger(LocalLibraryDirectory.class).fatal(ex.getMessage());
        }
        return movies;
    }
}
