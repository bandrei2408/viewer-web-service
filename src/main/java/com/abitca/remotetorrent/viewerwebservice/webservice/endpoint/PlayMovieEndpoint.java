package com.abitca.remotetorrent.viewerwebservice.webservice.endpoint;

import com.abitca.remotetorrent.viewerwebservice.ViewerWebService;
import com.abitca.remotetorrent.viewerwebservice.runtime.command.KillVLCCommand;
import com.abitca.remotetorrent.viewerwebservice.runtime.command.StartVLCCommand;
import com.abitca.remotetorrent.viewerwebservice.system.MovieFile;
import com.sun.net.httpserver.HttpExchange;
import org.apache.logging.log4j.LogManager;

import java.util.Map;

public class PlayMovieEndpoint extends GETEndpoint {

    @Override
    protected void handleRequest(HttpExchange exchange) {
        Map requestQuery = queryToMap(exchange.getRequestURI().getQuery());
        if(requestQuery == null || !requestQuery.containsKey("hash")) {
            send404(exchange);
            return;
        }
        MovieFile movieFile = ViewerWebService.library.getMovieByHash(requestQuery.get("hash").toString());
        if(movieFile == null){
            send404(exchange);
            return;
        }
        ViewerWebService.runtimeManager.runCommand(new KillVLCCommand());
        try {
            Thread.sleep(500);
        } catch (Exception ex){
            LogManager.getLogger(PlayMovieEndpoint.class).fatal(ex.getMessage());
        }
        ViewerWebService.runtimeManager.runCommand(new StartVLCCommand(movieFile.path));
        send200(exchange,"VLC started successfully!");
    }
}
