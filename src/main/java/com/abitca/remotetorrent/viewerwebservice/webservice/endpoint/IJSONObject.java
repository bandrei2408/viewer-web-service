package com.abitca.remotetorrent.viewerwebservice.webservice.endpoint;

import org.json.simple.JSONObject;

public interface IJSONObject {

    JSONObject toJSON();

}
