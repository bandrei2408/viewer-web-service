package com.abitca.remotetorrent.viewerwebservice.webservice.endpoint;

public abstract class POSTEndpoint extends MethodEndpoint {

    protected String getMethodType(){
        return "POST";
    }

}
