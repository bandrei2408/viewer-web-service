package com.abitca.remotetorrent.viewerwebservice.webservice.endpoint;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public abstract class MethodEndpoint implements HttpHandler {

    public void handle(HttpExchange exchange) throws IOException {
        if(!exchange.getRequestMethod().equals(getMethodType()))
        {
            send404(exchange);
            return;
        }
        handleRequest(exchange);
    }

    private void sendResponse(HttpExchange exchange, int responseStatus, String responseMessage){
        try {
            exchange.sendResponseHeaders(responseStatus, responseMessage.length());
            OutputStream os = exchange.getResponseBody();
            os.write(responseMessage.getBytes());
            os.close();
        } catch (IOException e) {
            LogManager.getLogger(MethodEndpoint.class).fatal(e.getMessage());
        }
    }

    protected void send404(HttpExchange exchange){
        sendResponse(exchange, 404, "404 Not Found");
    }

    protected void send200(HttpExchange exchange, String responseMessage){
        sendResponse(exchange, 200, responseMessage);
    }

    protected abstract String getMethodType();

    protected abstract void handleRequest(HttpExchange exchange);

    protected Map<String, String> queryToMap(String query){
        if(query == null) return null;
        Map<String, String> result = new HashMap<String, String>();
        for (String param : query.split("&")) {
            String pair[] = param.split("=");
            if (pair.length>1) {
                result.put(pair[0], pair[1]);
            }else{
                result.put(pair[0], "");
            }
        }
        return result;
    }

}
