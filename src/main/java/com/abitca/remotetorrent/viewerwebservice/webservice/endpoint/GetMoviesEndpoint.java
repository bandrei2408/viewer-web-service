package com.abitca.remotetorrent.viewerwebservice.webservice.endpoint;

import com.abitca.remotetorrent.viewerwebservice.ViewerWebService;
import com.sun.net.httpserver.HttpExchange;
import org.json.simple.JSONArray;

public class GetMoviesEndpoint extends GETEndpoint {

    @Override
    protected void handleRequest(HttpExchange exchange) {
        JSONArray moviesJSON = new JSONArray();
        ViewerWebService.library.getMovies().forEach( movie -> moviesJSON.add(movie.toJSON()));
        exchange.getResponseHeaders().set("Content-Type", "application/json");
        send200(exchange,moviesJSON.toJSONString());
    }

}
