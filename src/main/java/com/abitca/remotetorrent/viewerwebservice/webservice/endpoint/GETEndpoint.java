package com.abitca.remotetorrent.viewerwebservice.webservice.endpoint;

public abstract class GETEndpoint extends MethodEndpoint {

    protected String getMethodType(){
        return "GET";
    }

}
