package com.abitca.remotetorrent.viewerwebservice.webservice.endpoint;

import com.abitca.remotetorrent.viewerwebservice.ViewerWebService;
import com.abitca.remotetorrent.viewerwebservice.runtime.command.StartVLCCommand;
import com.sun.net.httpserver.HttpExchange;

public class StartVLCEndpoint extends GETEndpoint {

    public void handleRequest(HttpExchange exchange) {
        ViewerWebService.runtimeManager.runCommand(new StartVLCCommand("sample.avi"));
        send200(exchange,"VLC started successfully!");
    }

}
