package com.abitca.remotetorrent.viewerwebservice.webservice;

import com.abitca.remotetorrent.viewerwebservice.webservice.endpoint.GetMoviesEndpoint;
import com.abitca.remotetorrent.viewerwebservice.webservice.endpoint.PlayMovieEndpoint;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;

import org.apache.logging.log4j.LogManager;

public class WebService{

    private HttpServer httpServer;

    public WebService (int port){
        try{
            httpServer = HttpServer.create(new InetSocketAddress(port), 0);
            registerEndpoints();
            httpServer.setExecutor(null);
        } catch (IOException ex){
            LogManager.getLogger(WebService.class).fatal(ex.getMessage());
        }
    }

    public void start()
    {
        httpServer.start();
    }

    private void registerEndpoints(){
        httpServer.createContext("/play", new PlayMovieEndpoint());
        httpServer.createContext("/movies", new GetMoviesEndpoint());
    }

}
