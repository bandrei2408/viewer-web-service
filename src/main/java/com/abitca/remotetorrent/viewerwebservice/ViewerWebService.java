package com.abitca.remotetorrent.viewerwebservice;

import com.abitca.remotetorrent.viewerwebservice.runtime.RuntimeManager;
import com.abitca.remotetorrent.viewerwebservice.system.Configuration;
import com.abitca.remotetorrent.viewerwebservice.system.Library;
import com.abitca.remotetorrent.viewerwebservice.webservice.WebService;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;

public class ViewerWebService {

    public static WebService webService;
    public static RuntimeManager runtimeManager;
    public static Configuration configuration;
    public static Library library;

    public static void main(String[] args) {
        Options options = new Options();

        Option input = new Option("c", "config-file", true, "configuration file");
        input.setRequired(false);
        options.addOption(input);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            LogManager.getLogger(ViewerWebService.class).fatal(e.getMessage());
            formatter.printHelp("Viewer Web Service", options);
            return;
        }

        LogManager.getLogger(ViewerWebService.class).trace("Viewer Web Service started");
        configuration = new Configuration(cmd.getOptionValue("config-file"));
        library = new Library(configuration.getDirectories());
        library.syncMovies();
        runtimeManager = new RuntimeManager();
        webService = new WebService(8000);
        webService.start();
    }

}
