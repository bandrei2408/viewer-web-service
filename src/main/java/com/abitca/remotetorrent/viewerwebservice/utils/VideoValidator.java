package com.abitca.remotetorrent.viewerwebservice.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoValidator{

    private Pattern pattern;
    private Matcher matcher;

    private static final String VIDEO_PATTERN = "([^\\s]+(\\.(?i)(avi|AVI|wmv|WMV|flv|FLV|mpg|MPG|mp4|MP4|mkv|MKV))$)";

    public VideoValidator(){
        pattern = Pattern.compile(VIDEO_PATTERN);
    }

    public boolean validate(final String ext){
        matcher = pattern.matcher(ext);
        return matcher.matches();

    }
}