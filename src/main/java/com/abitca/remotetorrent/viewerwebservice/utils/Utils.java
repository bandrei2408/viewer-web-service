package com.abitca.remotetorrent.viewerwebservice.utils;

import org.apache.logging.log4j.LogManager;

import java.io.File;
import java.util.Scanner;

public class Utils {

    public static String readFile(String path){
        File configFile;
        Scanner scanner = null;
        String configuration = "";
        try{
            configFile = new File(path);
            scanner = new Scanner(configFile);
            configuration = scanner.useDelimiter("\\A").next();
        } catch (Exception ex){
            LogManager.getLogger(Utils.class).fatal(ex.getMessage());
        } finally {
            scanner.close();
            return configuration;
        }
    }

}
