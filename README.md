#Remote Torrent - Viewer Web Service

##Version v 0.0.3

####Supported players:
- VLC

####Supported movie libraries:
- Local library
- SMB Shared library

####Subtitles support: NO

###How to

On Server:

Run from cmd: java -jar ViewerWebService.jar --config-file {PATH\TO\CONFIG\FILE}

Configuration file structure:

```json
{
  "VLCPath" : "{VLC_PATH}",
  "library" : [
    {
      "type" : "LOCAL",
      "path" : "{DIRECTORY_PATH}"
    },
    {
      "type" : "SMBSHARE",
      "ip": "{SMB_IP}",
      "username": "{SMB_USERNAME}",
      "password": "{SMB_PASSWORD}",
      "path" : "{DIRECTORY_PATH}"
    }
  ]
}
```

On Client:

1. Go to http://{ip-address}:8000/movies
2. Get movie hash and play the movie with VLC via http://{ip-address}:8000/play?hash={hash}

Notes:
* Synchronization runs only when application is started (resynchronization will be supported in upcoming versions)
